CC=gcc
CFLAGS=-Wall -g
SRC=dwmline.c
OBJ=$(SRC:.c=.o)
HEAD=$(SRC:.c=.h)
EXEC=dwmline
LIBS=-lX11
INSTDIR=/usr/local/bin

default: $(EXEC)
$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) -o $(EXEC) $(OBJ) $(LIBS)

dwmline.o: dwmline.org
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "dwmline.org")'
	$(CC) -c -g dwmline.c

.PHONY: clean rebuild install

rebuild:
	make clean
	make

clean:
	rm -f $(EXEC)
	rm -f $(OBJ)
	rm -f $(SRC)
	rm -f $(HEAD)
	rm -f vgcore*

install:
	cp --force --verbose $(EXEC) $(INSTDIR)
	chmod 755 $(INSTDIR)/$(EXEC)
