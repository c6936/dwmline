#+AUTHOR: Jon Pina
#+DATE: <2023-05-12 Fri>
I'm calling this 'dwmline' because 'dwmstatus' is already taken, which is a much better application that this piece of shit.
#+begin_src c :tangle dwmline.c
#+end_src
* Header
** Includes
#+begin_src c :tangle dwmline.h
#ifndef DWMLINE_H
#define DWMLINE_H
#include <stdarg.h>
#+end_src
** Function Prototypes
#+begin_src c :tangle dwmline.h
char *get_datetime();
char *get_kernel();
int get_pacups();
char *smprintf(char *fmt, ...);
#endif
#+end_src
* Includes
- limits.h - to get min/max numerical values
- stdarg - variable arguments
#+begin_src c :tangle dwmline.c
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include "dbg.h"
#include "dwmline.h"
#+end_src
#+begin_src c :tangle dwmline.c
static Display *dpy;
#+end_src
* Helper Functions
** smprintf
Stolen from dwmstatus.
It takes a format string, then returns it as an allocate string.
#+begin_src c :tangle dwmline.c
char *smprintf(char *fmt, ...)
{
    va_list fmtargs;
    char *ret;
    int len;

    va_start(fmtargs, fmt);
    len = vsnprintf(NULL, 0, fmt, fmtargs);
    va_end(fmtargs);

    ret = malloc(++len);
    if(ret == NULL)
    {
      perror("memory error\n");
      exit(1);
    }

    va_start(fmtargs, fmt);
    vsnprintf(ret, len, fmt, fmtargs);
    va_end(fmtargs);
    return ret;
}
#+end_src
* Modules
Each of these functions return a value representing the current state of some system resource.
They should return the value only; leave formatting to main().
** Date & Time
Return current time.
Current format: YYYY-MM-DD HH:MM:SS
#+begin_src c :tangle dwmline.c
char *get_datetime()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    return smprintf("%d-%02d-%02d %02d:%02d:%02d",\
                    tm.tm_year + 1900,\
                    tm.tm_mon + 1,\
                    tm.tm_mday,\
                    tm.tm_hour,\
                    tm.tm_min,\
                    tm.tm_sec);
}
#+end_src
** Kernel
uname -r
#+begin_src c :tangle dwmline.c
char *get_kernel()
{
    char kernel[64] = { '\0' };
    FILE *fp;

    check(fp = popen("/usr/bin/uname -r", "r"), "popen error");
    fgets(kernel, sizeof(kernel), fp);
    pclose(fp);
    return smprintf("%s", kernel);
error:
    return NULL;
}
#+end_src
** Pacman Updates
Get the number of updates found by pacman.
The return value is the result of the shell command 'checkupdates | wc -l'.
Be sure to install 'pacman-contrib'.
#+begin_src c :tangle dwmline.c
int get_pacups()
{
    char buff[8] = { '0' };
    FILE *fp;

    check(fp = popen("/usr/bin/checkupdates | wc -l", "r"), "popen error");
    fgets(buff, sizeof(buff), fp);
    pclose(fp);
    return atoi(buff);
error:
    return -1;
}
#+end_src
* Main
If you're not using Arch, remove reference to pacups.
#+begin_src c :tangle dwmline.c
int main()
{
#+end_src
** Variable Definitions
| Variable | Value               |
|----------+---------------------|
| MODULE   |                     |
|----------+---------------------|
| datetime | YYYY-MM-DD HH:MM:SS |
| kernel   | current kernel      |
| pacups   | pacman updates      |
| status   | the final result    |
|----------+---------------------|
| MISC     |                     |
|----------+---------------------|
| dpy      | X Display           |
| i        | loop counter        |
*_int is the interval, or how many times per second the module will run.
#+begin_src c :tangle dwmline.c
    char *datetime;   int datetime_int = 1;
    char *kernel;     int kernel_int = INT_MAX-1; // int max
    int   pacups;     int pacups_int = 10;
    char *status;
    check(dpy = XOpenDisplay(NULL), "Cannot open display.");
    unsigned short int i;
#+end_src
** Variable Init
- I'm quadrupling the intervals because 'Main Loop' will run four times a second.
- We need to have the strings allocated going into the loop because I do not check if the string is already allocated, I just free it.
#+begin_src c :tangle dwmline.c
    datetime_int *= 4;
    pacups_int *= 4;

    datetime = malloc(1);
    kernel = malloc(1);
    pacups = -1;
    status = malloc(1);
    check(datetime && kernel && status, "malloc error");
#+end_src
** Main Loop
Here's where timing comes in.
#+begin_src c :tangle dwmline.c
    for(i = 0; 1 || sleep(.25); i++)
    {
#+end_src
*** Modules
Comment out unnecessary modules.
#+begin_src c :tangle dwmline.c
        if(i % datetime_int == 0)
        {
            free(datetime);
            datetime = get_datetime();
            printf("%s\n", datetime);
        }

        if(i % kernel_int == 0)
        {
          free(kernel);
          kernel = get_kernel();
          printf("%s\n", kernel);
        }

        if(i % pacups_int == 0)
        {
            pacups = get_pacups();
            printf("Updates: %d\n", pacups);
        }
#+end_src
*** Set Status
xsetroot -name
#+begin_src c :tangle dwmline.c
        free(status);
        status = smprintf("Updates: %d - Kernel: %s - %s", pacups, kernel, datetime);
        XStoreName(dpy, DefaultRootWindow(dpy), status);
        XSync(dpy, False);
    }
#+end_src
** Cleanup
Sentinel
#+begin_src c :tangle dwmline.c
    if(datetime)    free(datetime);
    if(kernel)      free(kernel);
    if(status)      free(status);
    XCloseDisplay(dpy);
    return 0;
error:
    exit(1);
}
#+end_src
